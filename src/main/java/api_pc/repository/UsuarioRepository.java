package api_pc.repository;

import api_pc.entities.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.Size;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Usuario findUsuarioByEmail(String email);
    Usuario findUsuarioByDni(String dni);



}
