package api_pc.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.File;
import java.io.Serializable;

@Entity
public class Coche implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String tipo; // Sera el tipo de vehiculo
    private String modelo; // Sera el modelo del coche
    private int anyo; // Sera le año de creacion del coche

    @Size(max = 20)
    @Column(unique = true)
    private String matricula; // Sera la matricula del coche

    private String imagen; // Sera la direccion url de la ubicacion de la api_pc.imagen

    @ManyToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_usuario") // campo o columna a crear en la tabla
    private Usuario id_usuario;

    // Builders
    public Coche() {
    }

    public Coche(String tipo, String modelo, int anyo, @Size(max = 20) String matricula, String imagen, Usuario id_usuario) {
        this.tipo = tipo;
        this.modelo = modelo;
        this.anyo = anyo;
        this.matricula = matricula;
        this.imagen = imagen;
        this.id_usuario = id_usuario;
    }

    // Setter and Getter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getImagen() {
        File ruta = new File(imagen);
        String rutaImagen = ruta.getAbsolutePath();
        return rutaImagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Usuario getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Usuario id_usuario) {
        this.id_usuario = id_usuario;
    }
}
