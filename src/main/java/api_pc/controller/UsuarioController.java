package api_pc.controller;

import api_pc.entities.Usuario;
import api_pc.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;


@RestController
@RequestMapping(path = "/api/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioRepository usuarioRepository;


    // Agrega un Usuario
    @PostMapping("/agregar")
    @ResponseBody
    public ResponseEntity<Usuario> addUser(@RequestBody Usuario usuario) {
        Usuario addUsuario = usuarioRepository.save(usuario);
        return ResponseEntity.status(HttpStatus.CREATED).body(addUsuario);
    }

    // Muestra todos los Usuarios
    @GetMapping("/mostrar")
    @ResponseBody
    public Iterable<Usuario> getAllUsers() {
        // Esto devuelve un JSON o XML con los usuarios.
        try {
            return usuarioRepository.findAll(); // devuelve la lista de usuarios
        } catch (DataIntegrityViolationException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No hay usuarios registrados");
        }
    }

    // Busca un Usuario por su dni
    @GetMapping("/mostrar/dni/{dniUsuario}")
    @ResponseBody
    public Usuario getUserByDni(@PathVariable String dniUsuario) {
        Usuario buscarUsuario = usuarioRepository.findUsuarioByDni(dniUsuario);
        if (buscarUsuario != null) {
            return buscarUsuario; // Devuelve un usuario buscado por su dni
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe el usuario.");
    }

    // Actualiza todos los datos por medio de id de usuario
    @PutMapping("/actualizar")
    public Usuario UpdateUser(@RequestBody Usuario usuario) {
        Optional<Usuario> buscarUsuarioBuscado = usuarioRepository.findById(usuario.getId());
        // Se verifica que el objeto exista y se asigna los nuevo valores
        if (buscarUsuarioBuscado.isPresent()){
            Usuario usuarioActualizar =  buscarUsuarioBuscado.get();
            usuarioActualizar.setNombre(usuario.getNombre());
            usuarioActualizar.setApellido(usuario.getApellido());
            usuarioActualizar.setDni(usuario.getDni());
            usuarioActualizar.setEmail(usuario.getEmail());
            usuarioActualizar.setTelefono(usuario.getTelefono());
            usuarioRepository.save(usuarioActualizar);
            return usuarioActualizar;
        }else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe el usuario a actualizar.");
        }
    }

    //==================================================================================================================
    //==================================================================================================================


    // Busca un Usuario por su email
    @GetMapping("/mostrar/email/{emailUsuario}")
    @ResponseBody
    public Usuario getUserByEmail(@PathVariable String emailUsuario) {
        Usuario buscarUsuario = usuarioRepository.findUsuarioByEmail(emailUsuario);
        if (buscarUsuario != null) {
            return buscarUsuario; // Devuelve un usuario buscado por su email
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe el usuario.");
    }

    // Eliminar un usuario buscado por su email y eliminado por su id
    @DeleteMapping("/eliminar/email/{emailUsuario}")
    public String deleteUserByEmail(@PathVariable String emailUsuario) {
        Usuario usuarioEliminarEmail = usuarioRepository.findUsuarioByEmail(emailUsuario);
        if (usuarioEliminarEmail != null) {
            this.usuarioRepository.deleteById(usuarioEliminarEmail.getId());
            return "Usuario Borrado.";
        } else return "El usuario no existe.";
    }


    // Eliminar un usuario buscado por su dni y eliminado por su id
    @DeleteMapping("/eliminar/dni/{dnilUsuario}")
    public String deleteUserByDni(@PathVariable String dnilUsuario) {
        Usuario usuarioEliminarDni = usuarioRepository.findUsuarioByDni(dnilUsuario);
        if (usuarioEliminarDni != null) {
            this.usuarioRepository.deleteById(usuarioEliminarDni.getId());
            return "Usuario Borrado.";
        } else return "El usuario no existe.";
    }

}
