package api_pc.controller;

import api_pc.entities.Coche;
import api_pc.entities.Usuario;
import api_pc.repository.CocheRepository;
import api_pc.repository.UsuarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(path = "/api/coche")
public class CocheController {

    private static final Logger log = LoggerFactory.getLogger(CocheController.class);

    @Autowired
    private CocheRepository cocheRepository;

    @Autowired
    private UsuarioRepository obtenerDatosUsuarioRepository;


    // Agrega un coche
    @PostMapping("/agregar")
    @ResponseBody
    public String addCoche(@RequestBody Coche coche) {
        /**Obtiene el id del usuario por medio del dni dado en el JSON de la variable id_usuario, dado que este es una
         * variable de tipo objeto, ademas del dni tambien contiene otros atributos de la clase 'Usuario'
         *
         * Aqui por medio de la variable objeto 'id_usuario' dentro de la clase 'Coche' se accede al dni del cliente
         *
         * coche.getId_usuario().getDni()
         *
         * para luego poder setearlo en coche.setId_usuario(usuarioBuscado)          */
        Usuario usuarioBuscado = obtenerDatosUsuarioRepository.findUsuarioByDni(coche.getId_usuario().getDni());
        try {
            coche.setId_usuario(usuarioBuscado); // Se asigna la clase del usuario e internamente le aplica el id de este
            cocheRepository.save(coche);
            return "Coche guardado en base de datos.";
        } catch (DataIntegrityViolationException e) {
            return "El Coche ya existe.";
        }
    }


    // Muestra todos los coches
    @GetMapping("/mostrar")
    @ResponseBody
    public Iterable<Coche> getAllCoches() {
        // Esto devuelve un JSON o XML con los coches.
        try {


//            MODIFICAR AQUI PARA PONER TURA DE IMAGEN EN PROGRAMA



            return cocheRepository.findAll(); // devuelve la lista de coches
        } catch (DataIntegrityViolationException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No hay usuarios registrados.");
        }
    }

    // Busca un coche por su matricula
    @GetMapping("/mostrar/matricula/{matriculaCoche}")
    @ResponseBody
    public Coche getCocheByMatricula(@PathVariable String matriculaCoche) {
        Coche buscarCoche = cocheRepository.findCocheByMatricula(matriculaCoche);
        if (buscarCoche != null) {
            return buscarCoche;
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe el coche.");
    }


    // Actualizar datos de coche, buscado por su matricula y se asigna el id de usuario buscado por su dni
    @PutMapping("/actualizar")
    public String UpdateCoche(@RequestBody Coche coche) {
        Coche buscaCocheActualizar = cocheRepository.findCocheByMatricula(coche.getMatricula()); // Se obtiene el coche

        if (buscaCocheActualizar != null) {
            buscaCocheActualizar.setTipo(coche.getTipo());
            buscaCocheActualizar.setModelo(coche.getModelo());
            buscaCocheActualizar.setAnyo(coche.getAnyo());
            cocheRepository.save(buscaCocheActualizar);
            return "Datos de coche actualizado.";
        } else return "No existe el coche que se quiere actualizar.";
    }


    // Elimina un coche buscado por su matricula y eliminado por su id
    @DeleteMapping("/eliminar")
    public String deleteCoche(@RequestParam String matricula) {
        Coche cocheEliminar = cocheRepository.findCocheByMatricula(matricula);
        if (cocheEliminar != null) {
            cocheRepository.deleteById(cocheEliminar.getId());
            return "Coche Borrado.";
        } else return "El coche a borrar no existe.";
    }

}
